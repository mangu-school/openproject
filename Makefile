MAKEFLAGS += --warn-undefined-variables
SHELL := bash
.SHELLFLAGS := -o errexit -o nounset -o pipefail -c
.DEFAULT_GOAL := run
.DELETE_ON_ERROR:
.SUFFIXES:

APP := openproject
ENVIRONMENT ?= dev

## DOCKER
## -------------------------------------------------------------------------------------
docker-run:
	docker run --rm -it \
		--name openproject-ce \
	  	--env OPENPROJECT_HOST__NAME=example.com \
  		--env OPENPROJECT_HTTPS=false \
  		--env OPENPROJECT_SECRET_KEY_BASE=secret \
		--volume $(PWD)/tmp/docker-run/pgdata:/var/openproject/pgdata \
		--volume $(PWD)/tmp/docker-run/assets:/var/openproject/assets \
		--publish 8184:80 \
		openproject/community:12.4.1

docker-shell:
	docker run --rm -it \
		--name openproject-ce \
	  	--env OPENPROJECT_HOST__NAME=example.com \
  		--env OPENPROJECT_HTTPS=false \
  		--env OPENPROJECT_SECRET_KEY_BASE=secret \
		--volume $(PWD)/tmp/docker-run/pgdata:/var/openproject/pgdata \
		--volume $(PWD)/tmp/docker-run/assets:/var/openproject/assets \
		--entrypoint=/bin/sh \
		openproject/community:12.4.1

## k8s
## -------------------------------------------------------------------------------------
build:
	mkdir -p manifests/builds/${ENVIRONMENT}
	kustomize build manifests/overlays/${ENVIRONMENT} -o manifests/builds/${ENVIRONMENT}/${APP}.yaml

dry-run:
	kubectl apply --wait=true --dry-run="client" -f manifests/builds/${ENVIRONMENT}/${APP}.yaml

deploy:
	kubectl apply --wait=true -f manifests/builds/${ENVIRONMENT}/${APP}.yaml

remove:
	kubectl delete --wait=true -f manifests/builds/${ENVIRONMENT}/${APP}.yaml
